#ifndef NETWORK_H
# define NETWORK_H
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <ctype.h>
#include <stdbool.h>

#include "world.h" // t_info, t_msg

#define MAX_TEAM_NAME 12
#define MSGPOOL_SIZE 1024 + 1

typedef struct s_opts
{
    int server_port;
    int width;
    int height;
    int team_num;
    char **teams;
    int max_clients;
    int div_time;
} t_opts;

typedef struct s_net_sys {
    int l_socket;
    int m_socket;
    fd_set master_set;
    int active_conn;
    int conn;
    bool monitor_conn;
} t_net_sys;

typedef struct s_dim {
    int x;
    int y;
} t_dim;

t_net_sys *init_network(int server_port, int monitor_port, int max_conn);
void destroy_network(t_net_sys *sys, int max_sd);
int update_from_network(t_net_sys *sys, int *max_sd);
int network_send(int to_socket, void *buf, size_t size);
int network_recv(t_net_sys *sys, int from_socket, char *buf, size_t size);

int init_options(t_opts *opts, int ac, char **av);
void destroy_options(t_opts *opts);
#endif
