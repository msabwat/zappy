#include "network.h"
#include "strings.h"

int main(int ac, char **av) {

	/*
      TODO: 
      init_world():
      - créer les ressources : done
      - initialiser les Joueurs
      - set pf_send() to ntwrk_snd (pf_send_mock pour tester)
      machine à état init
      
      init_options()
      init_network()
    */
    t_opts opts;
    if (init_options(&opts, ac, av)) {
        printf("error: could not initialize options\n");
        destroy_options(&opts);
        return 1;
    }
    t_net_sys *net_sys = init_network(opts.server_port,
                                      opts.server_port + 1,
                                      opts.max_clients);

    if (net_sys == NULL) {
        printf("error: could not initialize network\n");
        destroy_options(&opts);
        return 1;
    }

    int max_sd = net_sys->m_socket;
    // TODO: (mehdee) hide max_sd in sys
    // TODO: (mehdee) handle options errors (divider, big socket)
    // TODO: (mehdee) allow les reconnections des clients
    // TODO: (mehdee) écrire une classe pour network.py    
    destroy_network(net_sys, max_sd);
    destroy_options(&opts);
    return 0;
}
