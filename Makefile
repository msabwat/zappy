FLAGS 			= -g3 #@-fsanitize=address,undefined #-Wall -Wextra -Werror

CC				= gcc

INC				= -Iinclude 

NET_SRC_NAME	= network.c utils.c

WORLD_SRC_NAME	= world.c \
				  commands.c \
				  init.c \

NET_SRC_PATH	= network

WORLD_SRC_PATH	= world

NET_OBJ_NAME	= $(NET_SRC_NAME:.c=.o)

WORLD_OBJ_NAME	= $(WORLD_SRC_NAME:.c=.o)

OBJ_PATH		= .obj

WORLD_OBJ_PATH	= .obj/world

NET_OBJ_PATH	= .obj/network

WORLD_OBJS=$(addprefix $(OBJ_PATH)/world/,$(WORLD_OBJ_NAME)) 

NET_OBJS=$(addprefix $(OBJ_PATH)/network/,$(NET_OBJ_NAME)) 

OBJS=$(WORLD_OBJS) $(NET_OBJS)

all: makedir serveur

makedir:
	@mkdir -p .obj
	@mkdir -p .obj/world
	@mkdir -p .obj/network


test: FLAGS += -g3 -fsanitize=address,undefined
test: makedir serveur
	$(CC) $(FLAGS) tests/tests.c $(INC) $(OBJS) -o tests_runner

serveur: $(OBJ_PATH)/serveur.o $(OBJS)
	$(CC) $(FLAGS) $(OBJ_PATH)/serveur.o $(INC) $(OBJS) -o serveur

$(OBJ_PATH)/serveur.o: serveur.c
	$(CC) $(FLAGS)  $(INC) -c serveur.c -o $(OBJ_PATH)/serveur.o

$(WORLD_OBJ_PATH)/%.o: $(WORLD_SRC_PATH)/%.c
	$(CC) $(FLAGS)  $(INC) -c $< -o $@

$(NET_OBJ_PATH)/%.o: $(NET_SRC_PATH)/%.c
	$(CC) $(FLAGS)  $(INC) -c $< -o $@

install:
	curl -C - -LO "https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh"
	sh Miniconda3-latest-Linux-x86_64.sh -b -u -p `pwd`/miniconda3

clean:
	rm -fr $(OBJ_PATH)

fclean: clean
	rm -fr serveur
	rm -fr tests_runner
