sh_var=
sh_src=
if [ -n "$ZSH_VERSION" ]; then
    sh_var="bash"
    sh_src=".bash_profile"
elif [ -n "$BASH_VERSION" ]; then
    sh_var="zsh"
    sh_src=".zshrc"
else
    # bailout
    echo "please set sh_var= and sh_src="
    exit 1
fi

if [ -d "./miniconda3" ]; then
    ./miniconda3/bin/conda init "$sh_var"
    ./miniconda3/bin/conda config --set auto_activate_base false
    ./miniconda3/bin/conda create --name zappy python=3.7
    ./miniconda3/bin/conda activate zappy 
    ./miniconda3/bin/conda install -c cogsci pygame 
    source ~/"$sh_src"
else
    echo "error: missing miniconda3, please run make install!"
fi;
