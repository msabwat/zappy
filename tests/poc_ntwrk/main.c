#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <arpa/inet.h>

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <time.h>

#define MAX_CLIENTS_NUM 2

/*
  inspiré de: 
  https://www.ibm.com/docs/en/zos/2.1.0?topic=programs-c-socket-tcp-server
  https://www.ibm.com/docs/en/i/7.1?topic=designs-example-nonblocking-io-select

  Pour des tests successifs, il faut soit :
  - attendre la fin du TIME_WAIT (ie: la fin de la sessions TCP).
  - choisir un autre port si on est pressés.
  - SO_REUSEADDR

  Cf. schéma: 
  https://docs.microsoft.com/en-us/answers/questions/230227/time-wait-from-netstat.html
*/
bool term = false;

typedef struct s_client_msg {
    int client_count;
    int x;
    int y;
} t_client_msg;

void handle_sigint() {
    term = true;
}

int main() {
    /* prevent the server from ending if there is an issue */
	// signal(SIGPIPE, SIG_IGN);
    t_client_msg msg = {.client_count=0, .x=0, .y=0};
    int s = 0;
    signal(SIGINT, handle_sigint);
    int len, close_conn = 0;
    unsigned short port;
    char buf[12];
    bzero(buf, 12);
    struct sockaddr_in server;
    struct timeval timeout;
    fd_set master_set, working_set;
    int    desc_ready, end_server = 0;
    int new_sd;
    port = 9042;
    /*
     * Get a socket for accepting connections.
     */
    if ((s = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("Error could not create socket for accepting connections\n");
        exit(2);
    }

    int on = 1;
    
    if (setsockopt(s, SOL_SOCKET,  SO_REUSEADDR,
                   (char *)&on, sizeof(on)) < 0) {
        printf("could not setsockopt reuseaddr\n");
        exit(1);
    }    

    if (ioctl(s, FIONBIO, (char *)&on) < 0) {
        printf("could not set listening socket as non blocking\n");
        exit(1);
    }
    
    /*
     * Bind the socket to the server address.
     */
    server.sin_family = AF_INET;
    server.sin_port   = htons(port);
    server.sin_addr.s_addr = INADDR_ANY;
    if (bind(s, (struct sockaddr *)&server, sizeof(server)) < 0)
    {
        printf("Error could not bind socket for accepting connections\n");
        exit(3);
    }

    /*
     * Listen for connections. Specify the backlog as <nb of clients>.
     * At any point in time, we cannot have more than <nb of clients> 
     * waiting to connect (waiting for the accept call to complete).
     */
    if (listen(s, 128) != 0)
    {
        printf("Error, cannot listen to connections\n");
        exit(4);
    }
    // setup timeout for select call
    timeout.tv_sec  = 3 * 60;
    timeout.tv_usec = 0;

    // init fd set
    FD_ZERO(&master_set);
    int max_sd = s;
    FD_SET(s, &master_set);
    
    int ret = 0;
    int i = 0;
    time_t rawtime;
    struct tm * timeinfo;
    int client_count = 0;
    do {
        if (term == true) break;                
        time ( &rawtime );
        timeinfo = localtime ( &rawtime );
        printf ( "Il est : %s", asctime (timeinfo) );
        memcpy(&working_set, &master_set, sizeof(master_set));
        printf("Waiting on select()...\n");
        ret = select(max_sd + 1, &working_set, NULL, NULL, &timeout);
        if (ret < 0)
        {
            printf("  select() failed\n");
            break;
        }
        if (ret == 0)
        {
            printf("  select() timed out.\n");
            printf("  closing server.\n");
            break;
        }
        desc_ready = ret;
        for (i=0; i <= max_sd  &&  desc_ready > 0; ++i) {
            if (FD_ISSET(i, &working_set)) {
                desc_ready -= 1;
                if (i == s) {
                    printf("  Listening socket is readable\n");
                    new_sd = accept(s, NULL, NULL);
                    if (new_sd < 0)
                    {
                        if (errno != EWOULDBLOCK)
                        {
                            printf("  accept() failed\n");
                            end_server = 1;
                        }
                        else if (errno == EWOULDBLOCK)
                        {
                            printf("  accept() would block\n");
                        }
                    }
                    printf("  New incoming connection - %d\n", new_sd);
                    client_count +=1;
                    FD_SET(new_sd, &master_set);
                    if (new_sd > max_sd)
                        max_sd = new_sd;
                    if (client_count > MAX_CLIENTS_NUM) {
                        printf("connection refused \n");
                        ret = send(new_sd, "XXXXXXXXX", 10, 0);
                        if (ret) printf("sent closing msg to client\n");
                    }
                    else {
                        ret = send(new_sd, "BIENVENUE\n", 10, 0);
                        if (ret) printf("sent BIENVENUE to client\n");
                        /* it should not block */
                        ret = recv(new_sd, buf, sizeof(buf), 0);
                        if (ret) printf("received team name %s\n", buf);
                        // TODO: check if team exists
                        msg.client_count = 42;
                        msg.x = 10;
                        msg.y = 10;
                        ret = send(new_sd, &msg, sizeof(msg), 0);
                        if (ret) printf("sent msg to client\n");
                    }
                }
                else
                {
                    printf("  Descriptor %d is readable\n", i);
                    close_conn = 0;
                    printf("GOT A NEW COMMAND\n");                    
                    printf("checking if recv would block!\n");
                    ret = recv(i, buf, sizeof(buf), 0);
                    if (ret < 0)
                    {
                        if (errno != EWOULDBLOCK)
                        {
                            printf("  recv() failed\n");
                            close_conn = 1;
                        }
                        else if (errno == EWOULDBLOCK) {
                            printf("recv would block!\n");
                        }
                    }
                    else if (ret == 0)
                    {
                        printf("  Connection closed\n");
                        close_conn = 1;
                    }
                    else {
                        len = ret;
                        printf("  %d bytes received\n", len);
                        
                        ret = send(i, buf, len, 0);
                        if (ret < 0)
                        {
                            printf("  send() failed\n");
                            close_conn = 1;
                            if (errno == EWOULDBLOCK) {
                                printf("send would block\n");
                            }
                        }
                    }
                    if (close_conn)
                    {
                        close(i);
                        FD_CLR(i, &master_set);
                        if (i == max_sd)
                        {
                            while (FD_ISSET(max_sd, &master_set) == 0)
                                max_sd -= 1;
                        }
                    }
                }
            }
        }
    } while(end_server == 0);
    for (i=0; i <= max_sd; ++i)
    {
        if (FD_ISSET(i, &master_set))
            close(i);
    }
    printf("FIN!\n");
    exit(0);
}
