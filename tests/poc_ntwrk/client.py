import errno
import socket
import time
import struct
import sys

HOST = 'localhost'
PORT = 9042

# https://fiches-isn.readthedocs.io/fr/latest/FicheReseauxClient01.html

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect((HOST, PORT))
# instead of setting it to non blocking,
# and keep retrying until the socket is ready,
# we set it to blocking with a timeout, until
# the connection is established and we start the timer
client.settimeout(60 * 3)
# print 'Connexion vers ' + HOST + ':' + str(PORT) + ' reussie.'

welcome = client.recv(11)
if "X" in welcome:
    print("connection refused! max client number reached")
    sys.exit(1)

print(welcome)
client.send("team1")
answer = client.recv(struct.calcsize("i"))
print(struct.unpack('i', answer))
print("client ready to play!")
client.setblocking(0)
try:
    while True:
        try:            
            client.send(struct.pack('i10s', 1, "avance\n"));
#            client.send("client ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to playclient ready to play");
        except socket.error, e:
            if e.errno != errno.EAGAIN:
                raise e
            print("blocking")
        time.sleep(5)
except KeyboardInterrupt:
    client.shutdown()
    client.close()




