#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
/* 
   https://www.gnu.org/software/libc/manual/html_node/Example-of-Getopt.html 
   https://www.gnu.org/software/libc/manual/html_node/Using-Getopt.html
*/

typedef struct s_options {
    int server_port;
    int width;
    int height;
    int team_num;
    char **teams;
    int client_num;
    int div_time;
} t_options;

int check_mandatory_arg(bool _p, bool _x, bool _y,
                        bool _n, bool _c, bool _t) {
    if (_p == false) {
        printf("missing mandatory argument: port number\n");
        return 1;
    }
    else if (_x == false) {
        printf("missing mandatory argument: plateau width\n");
        return 1;
    }
    else if (_y == false) {
        printf("missing mandatory argument: plateau height\n");
        return 1;
    }
    else if (_n == false) {
        printf("missing mandatory argument: list of teams\n");
        return 1;
    }
    else if (_c == false) {
        printf("missing mandatory argument: max number of clients\n");
        return 1;
    }
    else if (_t == false) {
        printf("missing mandatory argument: time divider\n");
        return 1;
    }
    return 0;
}

int get_options(t_options *opts, int argc, char **argv)
{
  bool _p,_x,_y,_n,_c,_t = false;
  int index;
  int token;

  /* this makes getopt return '?' */
  opterr = 0;
  while ((token = getopt (argc, argv, "p:x:y:n:c:t:")) != -1)
  {
      switch (token)
      {
      case 'p':
          _p = true;
          opts->server_port = atoi(optarg);
          break;
      case 'x':
          _x = true;
          opts->width = atoi(optarg);
          break;
      case 'y':
          _y = true;
          opts->height = atoi(optarg);
          break;
      case 'n':
          _n = true;
          int num_team = 0;
          optind--;
          int ind = optind; 
          for( ;optind < argc && *argv[optind] != '-'; optind++){
              num_team += 1;
          }
          opts->team_num = num_team;
          opts->teams = (char **)malloc(sizeof(char *) * num_team);
          if (!opts->teams) {
              printf("error: failed to allocate list of teams\n");
              return 1;
          }
          for (int i = 0; i < num_team; i++) {
              opts->teams[i] = argv[i + ind];
          }
          break;
      case 'c':
          _c = true;
          opts->client_num = atoi(optarg);
          break;
      case 't':
          _t = true;
          opts->div_time = atoi(optarg);
          break;
      case '?':
          if ((optopt == 'p') || (optopt == 'x')
              || (optopt == 'y') || (optopt == 'n')
              || (optopt == 'c') || (optopt == 't'))
              fprintf (stderr, "Option -%c requires an argument.\n", optopt);
          else if (isprint (optopt))
              fprintf (stderr, "Unknown option `-%c'.\n", optopt);
          else
              fprintf (stderr,
                       "Unknown option character `\\x%x'.\n",
                       optopt);
          return 1;
      }
  }
  for (index = optind; index < argc; index++) {
    printf ("Non-option argument %s\n", argv[index]);
    return 1;
  }
  if (check_mandatory_arg(_p,_x,_y,_n,_c,_t))
      return 1;
  return 0;
}

int main (int ac, char **av) {
    t_options opts;
    if (get_options(&opts, ac, av)) {
        // print usage
        return 1;        
    }
    printf("### You selected the following options ###\n");
    printf("#~ Port number:%d\n", opts.server_port);
    printf("#~ Width:%d\n", opts.width);
    printf("#~ Height:%d\n", opts.height);
    printf("#~ Number of teams:%d\n", opts.team_num);

    printf("#~ Max clients allowed:%d\n", opts.client_num);
    printf("#~ Time divider:%d\n", opts.div_time);
    printf("###                                    ###\n");
    
    return 0;
}
