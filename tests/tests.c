#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "world.h"

t_cmd	cmd_tab[NB_CMD] =
{
	{"avance", 7, 0, &move},	
	{"droite", 7, 0},	
	{"gauche", 7, 0},	
	{"voir", 7, 0},	
	{"inventaire", 1, 0},	
	{"prend", 7, 1, &test_function},	
	{"pose", 7, 1},	
	{"expulse", 7, 0},	
	{"broadcast", 7, 1},	
	{"incantation", 300, 0},	
	{"fork", 42, 0},	
	{"connect_nbr", 0, 0},	
};

int main()
{
	t_sys	sys = {{0, 5, 5, 0, NULL, 0, 0}, 0, NULL, NULL, NULL};

	init_world(&sys);
}
