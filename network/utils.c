#include "network.h"
#include "world.h"

static int check_mandatory_arg(bool _p, bool _x, bool _y,
                        bool _n, bool _c, bool _t) {
    if (_p == false) {
        printf("missing mandatory argument: port number\n");
        return 1;
    }
    else if (_x == false) {
        printf("missing mandatory argument: plateau width\n");
        return 1;
    }
    else if (_y == false) {
        printf("missing mandatory argument: plateau height\n");
        return 1;
    }
    else if (_n == false) {
        printf("missing mandatory argument: list of teams\n");
        return 1;
    }
    else if (_c == false) {
        printf("missing mandatory argument: max number of clients\n");
        return 1;
    }
    else if (_t == false) {
        printf("missing mandatory argument: time divider\n");
        return 1;
    }
    return 0;
}

static int check_atoi(const char *str, int i) {
    if (i == 0) {
        printf("input error: %s is too large\n", str);
        return 1;
    }
    return 0;
}

int init_options(t_info *opts, int argc, char **argv)
{
  bool _p,_x,_y,_n,_c,_t = false;
  int index;
  int token;

  /* this makes getopt return '?' */
  opterr = 0;
  int ret = 0;
  while ((token = getopt (argc, argv, "p:x:y:n:c:t:")) != -1)
  {
      switch (token)
      {
      case 'p':
          _p = true;
          opts->server_port = atoi(optarg);
          if (check_atoi("port", opts->server_port))
              return 1;
          break;
      case 'x':
          _x = true;
          opts->width = atoi(optarg);
          if (check_atoi("width", opts->width))
              return 1;                    
          break;
      case 'y':
          _y = true;          
          opts->height = atoi(optarg);
          if (check_atoi("height", opts->height))
              return 1;                    
          break;
      case 'n':
          _n = true;
          int num_team = 0;
          optind--;
          int ind = optind; 
          for( ;optind < argc && *argv[optind] != '-'; optind++){
              num_team += 1;
          }
          opts->team_num = num_team;
          opts->teams = (char **)malloc(sizeof(char *) * num_team);
          if (!opts->teams) {
              printf("error: failed to allocate list of teams\n");
              return 1;
          }
          for (int i = 0; i < num_team; i++) {
              opts->teams[i] = argv[i + ind];
          }
          break;
      case 'c':
          _c = true;
          opts->max_clients = atoi(optarg);
          if (check_atoi("max_clients", opts->max_clients))
              return 1;
          break;
      case 't':
          _t = true;
          opts->div_time = atoi(optarg);
          if (check_atoi("div_time", opts->div_time))
              return 1;                    

          break;
      case '?':
          if (strchr("pxynct", optopt) != NULL)
              fprintf (stderr, "Option -%c requires an argument.\n", optopt);
          else if (isprint (optopt))
              fprintf (stderr, "Unknown option `-%c'.\n", optopt);
          else
              fprintf (stderr,
                       "Unknown option character `\\x%x'.\n",
                       optopt);
          return 1;
      }
  }
  for (index = optind; index < argc; index++) {
    printf ("Non-option argument %s\n", argv[index]);
    return 1;
  }
  if (check_mandatory_arg(_p,_x,_y,_n,_c,_t))
      return 1;
  return 0;
}

void destroy_options(t_info *opts) {
    if (opts) {
        if (opts->teams) {
            free(opts->teams);
        }
    }
}
