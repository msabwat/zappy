#include "network.h"

static int setup_socket(int socket, int port) {
    int on = 1;
    if ( setsockopt(socket, SOL_SOCKET, SO_REUSEADDR,
                   (char*) &on, sizeof(on)) < 0 ) {
        printf("error: could not set socket as reusable\n");
        return 1;
    }

    if (ioctl( socket, FIONBIO, (char*)&on) < 0 ) {
        printf("error: could not set socket as non blocking\n");
        return 1;
    }

    struct sockaddr_in server;
    server.sin_family = AF_INET;
    server.sin_port   = htons(port);
    server.sin_addr.s_addr = INADDR_ANY;
    if (bind(socket, (struct sockaddr *)&server, sizeof(server)) < 0) {
        printf("error: could not bind socket\n");
        return 1;
    }
    /*
     * Listen for connections. Specify the backlog as <nb of clients>.
     * At any point in time, we cannot have more than <nb of clients> 
     * waiting to connect (waiting for the accept call to complete).
     */
    if ( listen(socket, 128) != 0 ) {
        printf("error: cannot listen to connections\n");
        return 1;
    }
    return 0;
}

t_net_sys *init_network(int server_port, int monitor_port, int max_conn) {
    /* 
       setup listening socket for a tcp server,
       and socket for the monitor.              
    */
    t_net_sys *sys = (t_net_sys *)malloc(sizeof(t_net_sys));
    if (!sys) {
        printf("error: no memory\n");
        return NULL;
    }

    if (( sys->l_socket = socket(AF_INET, SOCK_STREAM, 0)) < 0 ) {
        printf("error: could not create tcp server socket\n");
        free(sys);
        return NULL;
    }
    if (( sys->m_socket = socket(AF_INET, SOCK_STREAM, 0)) < 0 ) {
        printf("error: could not create monitor socket\n");
        free(sys);
        return NULL;
    }
    if ( setup_socket(sys->l_socket, server_port) == 1 ) {
        printf("error: failed to setup clients socket\n");
        close(sys->l_socket);
        free(sys);
        return NULL;
    }
    if ( setup_socket(sys->m_socket, monitor_port) == 1 ) {
        printf("error: failed to setup monitor socket\n");
        close(sys->m_socket);
        free(sys);
        return NULL;
    }

    FD_ZERO(&sys->master_set);
    FD_SET(sys->m_socket, &sys->master_set);
    FD_SET(sys->l_socket, &sys->master_set);
    sys->conn = max_conn;
    sys->active_conn = 0;
    sys->monitor_conn = false;
    return sys;
}

void destroy_network(t_net_sys *sys, int max_sd) {
    for (int i=0; i <= max_sd; ++i) {
        if (FD_ISSET(i, &sys->master_set))
            close(i);
    }

    if (sys) {
        free(sys);
    }
}

static int select_fds(fd_set *working_set, int max_sd) {
    int ret;

    /* 
       TODO: add a timeout 
       depending on if we go with plan a or b :p
    */
    ret = select(max_sd + 2, working_set, NULL, NULL, NULL);
    if (ret < 0) {
        printf("select failed(), closing server!\n");
        return -1;
    }
    else if (ret == 0) {
        printf("select timedout, closing server!\n");
        return -1;
    }
    return ret;
}

int network_send(int to_socket, void *buf, size_t size) {
    printf("network send!\n");
    int ret = send(to_socket, buf, size, 0);
    if (ret == -1) {
        if (errno == EWOULDBLOCK) {
            printf("send() would block!\n");
            return 0;
        }
        else {
            printf("send() failed with an error\n");
            return 1;
        }
    }
    return ret;
}

int network_recv(t_net_sys *sys, int from_socket, char *buf, size_t size) {
    int ret = recv(from_socket, buf, size, 0);
    printf("received %s\n", buf);
    buf[ret] = '\0';
    if (ret == -1) {
        if (errno == EWOULDBLOCK) {
            printf("recv() would block!\n");
            return 0;
        }
        else if (errno == ECONNRESET) {
            // TODO: check other error codes that indicate termination
            printf("connection from socket: %d was reset\n", from_socket);
            // TODO: check if it should still be alive, before you decrement
            sys->active_conn -= 1;
            return 0;
        }
        else {
            printf("recv() failed with a fatal error:%s\n", strerror(errno));
            return 1;
        }
    }
    return ret;
}
/* handle monitor/server dialog */
static int handle_monitor_heartbeat(t_net_sys *sys, int *max_sd) {
    int new_sd = accept(sys->m_socket, NULL, NULL);
    if (new_sd < 0) {
        if (errno != EWOULDBLOCK) {
            printf("accept() failed\n");
            // close the server.
            return 1;
        }
        else if (errno == EWOULDBLOCK) {
            printf("accept() would block\n");
            // try again.
            return 0;
        }        
    }
    FD_SET(new_sd, &sys->master_set);
    if (new_sd > *max_sd) {
        *max_sd = new_sd;
    }
    int ret = 0;
    ret = network_send(new_sd, "BIENVENUE\n", 10);
    if (ret == 1) goto error;
    char msg[42];
    ret = recv(new_sd, msg, sizeof(msg), 0);
    if (ret == 1) goto error;
    msg[ret]='\0';
    if (strncmp(msg, "GRAPHIC\n", 8) == 0) {
        // connection established
        sys->monitor_conn = true;
        // TODO: get info from world
        printf("SENDING payloads to monitor...\n");
        return 0;
    }
    return 0;
       
error:
    printf("network send/recv failed! leaving...\n");
    return 1;   

}

/* handle client/server dialog */
static int handle_clts_heartbeat(t_net_sys *sys, int *max_sd, int msgs_size) {
    int new_sd = accept(sys->l_socket, NULL, NULL);
    if (new_sd < 0) {
        if (errno != EWOULDBLOCK) {
            printf("accept() failed\n");
            // close the server.
            return 1;
        }
        else if (errno == EWOULDBLOCK) {
            printf("accept() would block\n");
            // try again.
            return 0;
        }
    }
    // accept succeeded \o/
    sys->conn -= 1;
    sys->active_conn += 1;
    FD_SET(new_sd, &sys->master_set);
    if (new_sd > *max_sd) {
        *max_sd = new_sd;
    }
    int ret = 0;
    /*
      TODO: in order to allow reconnections, 
      sys->conn, should be computed from the world too
    */
    if (sys->conn <= -1) {
        printf("connection refused\n");
        ret = network_send(new_sd, "XXXXXXXXXX", 10);
        if (ret == 1) goto error;
        printf("sent %d bytes\n", ret);
        close(new_sd);
        FD_CLR(new_sd, &sys->master_set);
    }
    else {
        ret = network_send(new_sd, "BIENVENUE\n", 10);
        if (ret == 1) goto error;
        char team_name[MAX_TEAM_NAME];
        ret = recv(new_sd, team_name, sizeof(team_name), 0);
        if (ret == 1) goto error;
        ret = network_send(new_sd, &sys->conn, sizeof(sys->conn));
        if (ret == 1) goto error;
        t_dim dim = (t_dim) { .x=10, .y=10 };
        ret = network_send(new_sd, &dim, sizeof(dim));
        if (ret == 1) goto error;
    }
    return 0; // don't kill the server
error:
    printf("network send/recv failed! leaving...\n");
    return 1;   
}

static t_inventory object_from_str(char *str) {
    if (strncmp(str, "linemate\n", 9) == 0) {
        return linemate;
    }
    else if (strncmp(str, "deraumere\n", 10) == 0) {
        return deraumere;
    }
    else if (strncmp(str, "sibur\n", 6) == 0) {
        return sibur;
    }
    else if (strncmp(str, "mendiane\n", 9) == 0) {
        return mendiane;
    }
    else if (strncmp(str, "phiras\n", 7) == 0) {
        return phiras;
    }
    else if (strncmp(str, "thystame\n", 9) == 0) {
        return thystame;
    }
    else {
        // is it yummy in my tummy? :)
        // TODO?
    }
    return -1;
}

enum cmd {
    object,
    broadcast_message
};

static int parse_cmd_param(enum cmd command, char *message, int *param, char *b_msg) {
    if (command == object) {
        /* if we expect an object, there will be only one separator */
        char *tkn = strtok(message, " ");
        tkn = strtok(NULL, " "); // here we get the object
        t_inventory o = object_from_str(tkn);
        if (o == -1) {
            return 1;
        }
        tkn = strtok(NULL, " "); // check if the message is not malformed
        if (tkn == NULL) {
            return 1;
        }
        *param = o;
    }
    else if (command == broadcast_message) {
        char *tkn = strtok(message, " ");
        tkn = strtok(NULL, " "); // here we get the message
        if (strchr(tkn, '\n') == NULL) {
            printf("missing \n in message\n");
            return 1;
        }
        size_t n = strlen(tkn);
        b_msg = (char*)malloc(n);
        if (!b_msg) {
            printf("enomem: cannot allocate memory\n");
            return 1;
        }        
        strncpy(b_msg, tkn, n);
        tkn = strtok(NULL, "\n"); // check if the message is not malformed
        if (tkn == NULL) {
            free(b_msg);
            return 1;
        }
    }
    return 0;
}
/* 
   If parse_cmd_param does not fail, 
   we should free broadcast_msg when 
   we pop the message from the pool.
*/
static int (*runner_from_msg(char *message, int *param, char *b_msg))(t_entity *) {
    unused(param);
    unused(b_msg);
    
    if (strncmp(message, "avance\n", 7) == 0) {
        return &move;
    }
    else if (strncmp(message, "droite\n", 7) == 0) {
        return &rotate_right;
    }
    else if (strncmp(message, "gauche\n", 7) == 0) {
        return &rotate_left;
    }
    else if (strncmp(message, "voir\n", 5) == 0) {
        return &vision;
    }
    else if (strncmp(message, "inventaire\n", 11) == 0) {
        return &inventory;
    }
    else if (strncmp(message, "prend ", 6) == 0) {        
        if (parse_cmd_param(object, message, param, b_msg)) {            
            return NULL;
        }
        return &take_object;
    }
    else if (strncmp(message, "pose ", 5) == 0) {
        if (parse_cmd_param(object, message, param, b_msg)) {            
            return NULL;
        }
        return &put_object;
    }
    else if (strncmp(message, "expulse\n", 8) == 0) {
        return &expulse;
    }
    else if (strncmp(message, "broadcast ", 10) == 0) {
        if (parse_cmd_param(broadcast_message, message, param, b_msg)) {
            return NULL;
        }
        return &broadcast;
    }
    else if (strncmp(message, "incantation\n", 12) == 0) {
        return &start_incantation;
    }
    else if (strncmp(message, "fork\n", 5) == 0) {
        return &fork_player;
    }
    else if (strncmp(message, "connect_nbr\n", 12) == 0) {
        return &get_remaining_connections;
    }
    return NULL;
}

static int add_msg(int socket, t_msg *msg, int msgs_size, char *message, int size) {
    unused(socket);
    unused(message);
    unused(size);
    unused(msg);
    int index = 0;
    for (int i = 0; i < msgs_size; i++) {
        if (msg[index].id == 0) {            
            index = i;
            break;
        }
    }
    int param = 0;
    char *b_msg = NULL;
    int (*run)(t_entity *) = runner_from_msg(message, &param, b_msg);
    if (run) {
        msg[index].id = socket;
        msg[index].run = run;
        msg[index].param = 0;
        msg[index].broadcast_msg = b_msg;
    }
    else {
        printf("received bad message, ignoring...\n");
        return 1;
    }
    return 0;
}

static int handle_monitor_msg(int socket, char *message, int size) {
    unused(socket);
    unused(message);
    unused(size);
    
    return 0;
}

static int handle_commands(t_msg *msg, t_net_sys *sys, int socket, int nb_m) {
    int ret = 0;
    char message[42];
    ret = recv(socket, message, sizeof(message), 0);
    message[ret] = '\0';
    if (ret == 1) goto error;
    else if (ret == 0) goto error;
    else {
        message[ret] = '\0';
        if (add_msg(socket, msg, nb_m, message, ret)) {
            printf("debug: received a bad message from a client: %s\n", message);
        }
        if (handle_monitor_msg(socket, message, ret)) {
            printf("received a bad message from the monitor: %s\n", message);
        }
    }

    return 0;
error:
    printf("network send/recv failed! leaving...\n");
    return 1;
}

int update_from_network(t_net_sys *sys, int *max_sd) {
    int end_server = 0;
    int desc_ready = 0;
    int fds = 0;
    fd_set working_set;

    memcpy(&working_set, &sys->master_set, sizeof(sys->master_set));
    fds = select_fds(&working_set, *max_sd);
    if (fds == -1)
        return 1;
    bool size_changed = false;
    if (fds > MSGPOOL_SIZE) {
        msg = realloc(msg, fds);
        if (!msg) {
            printf("enomem: could not realloc\n");
            return 1;
        }
        size_changed = true;
    }
    desc_ready = fds;
    /* monitor socket */
    int s = 0;
    int nb_msgs = 0;
    if (size_changed == true) {
        nb_msgs = fds;
    }
    else {
        nb_msgs = MSGPOOL_SIZE;
    }
    for (int i = 0; i <= *max_sd && desc_ready > 0; i++) {
        if (FD_ISSET(i, &working_set)) {
            desc_ready -= 1;
            if (i == sys->l_socket) {
                printf("connection request from client\n");
                if (handle_clts_heartbeat(sys, max_sd, msg, nb_msgs)) {
                    return 1;
                }
            }
            else if (i == sys->m_socket) {
                printf("connection request from monitor\n");
                if (handle_monitor_heartbeat(sys, max_sd)) {
                    return 1;
                }
            }
            else {
                if (handle_commands(msg, sys, i, nb_msgs)) {
                    close(i);
                    FD_CLR(i, &sys->master_set);
                    if (i == *max_sd)
                    {
                        while (FD_ISSET(*max_sd, &sys->master_set) == 0)
                            *max_sd -= 1;
                    }
                    return 1;
                }
            }
        }
    }
    if ((sys->active_conn == 0) && (sys->monitor_conn == false)) {
        printf("error: no more active connections!\n");
        return 1;
    }   
    return end_server;
}
