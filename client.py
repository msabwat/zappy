import sys
import os
from ia import *
from network import *

if __name__ == '__main__':
    try:
        e = os.environ['CONDA_DEFAULT_ENV']
        if ( e != "zappy"):
            raise ValueError("error, wrong env ! Please run :")
    except (KeyError,ValueError) :
        print("error, wrong env ! Please run :")
        print("  source setup_python.sh")
        print("  conda activate zappy")
        sys.exit(1)
    sys.exit(0)
