import sys
from graphics import *
from network import *

def get_base_prefix_compat():
    """Get base/real prefix, or sys.prefix if there is none."""
    return getattr(sys, "base_prefix", None) or getattr(sys, "real_prefix", None) or sys.prefix

def in_virtualenv():
    return get_base_prefix_compat() != sys.prefix

if __name__ == '__main__':
    if (not in_virtualenv()):
        print("Not in venv ! Please run :")
        print("  python3 -m venv .")
        print("  source bin/activate")
        sys.exit(1)
    sys.exit(0)
